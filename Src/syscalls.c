/**
*****************************************************************************
**
**  File        : syscalls.c
**
**  Abstract    : System Workbench Minimal System calls file
**
** 		          For more information about which c-functions
**                need which of these lowlevel functions
**                please consult the Newlib libc-manual
**
**  Environment : System Workbench for MCU
**
**  Distribution: The file is distributed as is, without any warranty
**                of any kind.
**
**  (c)Copyright System Workbench for MCU.
**  You may use this file as-is or modify it according to the needs of your
**  project. Distribution of this file (unmodified or modified) is not
**  permitted. System Workbench for MCU permit registered System Workbench(R) users the
**  rights to distribute the assembled, compiled & linked contents of this
**  file as part of an application binary file, provided that it is built
**  using the System Workbench for MCU toolchain.
**
*****************************************************************************
*/

/* Includes */
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>

#ifdef __GNUC__
#include <sys/unistd.h>
#endif

#include "usbd_cdc_if.h"

#ifndef __GNUC__
#define EINVAL 22
#define STDOUT_FILENO 1
#define STDERR_FILENO 2
#define EBADF 9
#endif

/* Functions */
int _read (int file, char *ptr, int len)
{
//	int DataIdx;
//
//	for (DataIdx = 0; DataIdx < len; DataIdx++)
//	{
//		*ptr++ = __io_getchar();
//	}

return len;
}

int _write(int file, char *ptr, int len)
{
    uint8_t ret = USBD_OK;
    switch (file)
    {
    case STDOUT_FILENO: /*stdout*/
        ret = CDC_Transmit_FS((uint8_t*)ptr, len);
        break;
    case STDERR_FILENO: /* stderr */
        ret = CDC_Transmit_FS((uint8_t*)ptr, len);
        break;
    default:
        errno = EBADF;
        return -1;
    }
    if (ret == USBD_OK)
        return len;
    if (ret == USBD_BUSY)
        return 0;
    errno = EBADF;
    return -1;
}
